function selectLink(link) {
  var allLinks = Array.from(document.getElementById("portfolio-filter").getElementsByTagName("a"));

  allLinks.forEach(function (element) {
    element.classList.remove("filter__link--active");
  });

  link.classList.add("filter__link--active");
}

function showAllInPortfolio(link) {
  var portfolioItems = document.querySelectorAll("[data-type]");
  portfolioItems.forEach(function (element) {
    element.classList.remove("portfolio__item--hidden");
  });

  selectLink(link);

  return false;
}

function showInPortfolio(link, type) {
  var portfolioItems = document.querySelectorAll("[data-type]");
  portfolioItems.forEach(function (element) {
    if (!element.getAttribute("data-type").includes(type)) {
      element.classList.add("portfolio__item--hidden");
    } else {
      element.classList.remove("portfolio__item--hidden");
    }
  });

  selectLink(link);

  return false;
}

/**** Carousel ****/

$('#recipeCarousel').carousel({
  interval: 10000
})

$('.carousel .carousel-item').each(function () {
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));

  if (next.next().length > 0 && next.next().next().length > 0) {
    next.next().children(':first-child').clone().appendTo($(this));
    next.next().next().children(':first-child').clone().appendTo($(this));
    
  } else if (next.next().length > 0){
    next.next().children(':first-child').clone().appendTo($(this));
    $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
    
  } else {
    $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
    $(this).siblings(':first').next().children(':first-child').clone().appendTo($(this));
  }
});

/**** /Carousel ****/
